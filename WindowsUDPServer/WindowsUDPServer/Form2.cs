﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUDPServer

{

    public partial class Form2 : Form
    {
        int portNumTemp;
        public Form2()
        {
            InitializeComponent();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Please enter only valid numbers");
                textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1);
            }
                
        }

        private void button1_Click(object sender, EventArgs e)
        {
            portNumTemp = Convert.ToInt32(textBox1.Text);
            if (portNumTemp < 0 || portNumTemp > 65535)
            {
                MessageBox.Show("Please enter a port number between 0 and 65535.");
            }
            else
            {
                Form1.portNumber = portNumTemp;
                this.Close();
            }
        }
    }
}
