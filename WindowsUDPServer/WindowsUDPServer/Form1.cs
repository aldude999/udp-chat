﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsUDPServer
{
    public partial class Form1 : Form
    {
        public static int portNumber = 11000;
        bool startStop = false;
        StringBuilder tbText = new StringBuilder();
        public Thread childServer;
        System.Net.Sockets.UdpClient udpServer;
        System.Net.IPEndPoint EndPoint;
        byte[] data = new byte[0] ;
		byte[] tempData = new byte[0];
        string dataString;
        string[] dataArray = new string[1];
        Int32 userID = 0;
        List<System.Net.IPAddress> Addresses = new List<System.Net.IPAddress>();
        List<Int32> Ports = new List<Int32>();
        List<Int32> kickUser = new List<Int32>();
        public Form1()
        {

            
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (portNumber < 0 || portNumber > 65535)
            {
                MessageBox.Show("Please enter a port number.");
            }
            else
            {
                startStop = true;
                serverControl();

            }
        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Environment.Exit(0);
        }

        private void portToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
        private void serverMethod()
        {
            EndPoint = new System.Net.IPEndPoint(System.Net.IPAddress.Any, portNumber);
            while (startStop)
            {
                
                    


                Application.DoEvents();
                bool sendMessage = false;
                try
                {



                    receiveData();





                    //tbText.AppendLine("receive data from " + remoteEP.ToString());
                    for (int i = Addresses.Count - 1; i >= 0; i--)
                    {
                        if (EndPoint.Address.Equals(Addresses[i]) && EndPoint.Port == Ports[i] && data.Length != new byte[0].Length && dataArray[0] == "CHAT")
                        {
                            tbText.AppendLine(dataArray[1]);
                            sendMessage = true;
                        }
                        kickCheck(i, EndPoint);
                    }
                    sendMessage = messageCheck();
                    for (int i = Addresses.Count - 1; i >= 0; i--)
                    {
                        var sendPoint = new System.Net.IPEndPoint(Addresses[i], Ports[i]);

                        if (data.Length != new byte[0].Length && sendMessage == true && sendMessage == true)
                        {
                              //Thread childSender = new Thread(() => sendData(i, sendPoint));
                             //childSender.Start();
                            sendData(i, sendPoint);
                            //tbText.AppendLine("Sent packet back!");
                        }
                    }
                }

                catch (Exception ex)
                {
                    toolStripStatusLabel1.Text = ex.Source;
                }



                

            }
            udpServer.Close();
            tbText.AppendLine("Server successfully stopped!");
            System.Threading.Thread.Sleep(90);
            timer1.Stop();
        }
        private void serverControl()
        {

            if (startStop == true)
            {
            udpServer = new System.Net.Sockets.UdpClient(portNumber);
            tbText.AppendLine("Server Created on port: " + Convert.ToString(portNumber));
            udpServer.Client.ReceiveTimeout = 500;
                childServer = new Thread(serverMethod);
                childServer.Start();
                button1.Enabled = false;
                timer1.Interval = 60;
                timer1.Start();
                stopServerToolStripMenuItem.Enabled = true;
            }
            else
            {
                button1.Enabled = true;
                stopServerToolStripMenuItem.Enabled = false;
            }

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (textBox1.Text != tbText.ToString())
            {
                textBox1.Text = tbText.ToString();
                textBox1.Invalidate();
                textBox1.Update();
                textBox1.Refresh();
                textBox1.SelectionStart = textBox1.TextLength;
                textBox1.ScrollToCaret();
                Application.DoEvents();
            }
        }

        private void stopServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startStop = false;
            serverControl();
        }

        private void stop_Timer_Tick(object sender, EventArgs e)
        {

        }
        private string byte2String(byte[] convertMe)
        {
            return Encoding.UTF8.GetString(convertMe);
        }
        private byte[] string2Byte(string convertMe)
        {
            return Encoding.UTF8.GetBytes(convertMe);
        }
		private bool messageCheck()
		{
			if (dataArray[0] == "CONNECT FRM V.4")
                        {
                            Ports.Add(EndPoint.Port);
                            Addresses.Add(EndPoint.Address);
                            kickUser.Add(0);
                            data = new byte[0];
                            udpServer.Send(string2Byte("RCVD"), string2Byte("RCVD").Length);
                        }
            if (dataArray[0] == "DISCONNECT")
                        {
                            Ports.IndexOf(EndPoint.Port);
                            data = new byte[0];
                        }
            if (dataArray[0] == "PING")
            {
                tbText.AppendLine("Received PING packet!");
                tbText.AppendLine("Sending PONG packet!");
                udpServer.Send(string2Byte("PONG"), string2Byte("PONG").Length, EndPoint);
            }
            if (dataArray[0] == "LIST")
            {
                tbText.AppendLine("Sending Userlist!");
                string userList;
                userList = "LIST";
                for (Int32 i = Addresses.Count - 1; i >= 0; i--)
                {
                    userList = userList + "\n" + Addresses[i] + ":" + Ports[i];
                }
                udpServer.Send(string2Byte(userList), string2Byte(userList).Length, EndPoint);
            }
            if (dataArray[0] == "CHAT")
            {
                
                return true;
            }
            else
            {
                return false;
            }
		}
		private void sendData(Int32 i, System.Net.IPEndPoint endPoint)
		{
            udpServer.Send(string2Byte(dataString), string2Byte(dataString).Length, endPoint); // reply back
            receiveCheck(i, endPoint);
            
        }
            private void receiveCheck(Int32 i, System.Net.IPEndPoint endPoint)
            {
                try
                {
                    if (byte2String(udpServer.Receive(ref endPoint)) == "RCVD")
                    {
                       // return i;
                    }
                    else
                    {
                        Int32 LineItem = Ports.IndexOf(endPoint.Port);
                        Addresses.RemoveAt(LineItem);
                        Ports.RemoveAt(LineItem);
                        kickUser.RemoveAt(LineItem);
                        tbText.AppendLine("Removed dead user...");
                        //return i--;
                    }
                }
                catch (System.Net.Sockets.SocketException ex)
                {
               //     try
                //    {
                    Int32 LineItem = Ports.IndexOf(endPoint.Port);
                        Addresses.RemoveAt(LineItem);
                        Ports.RemoveAt(LineItem);
                        kickUser.RemoveAt(LineItem);
                        tbText.AppendLine("Removed dead user...");
                 //       //return i--;
                    }
                 //   catch (Exception ex2) {
                    //return i;
                    }

                
     
           
		
		private void kickCheck (Int32 i, System.Net.IPEndPoint endPoint)
		{
		if (kickUser[i] >= 3)
            {
            data = string2Byte("DISCONNECT");
            udpServer.Send(data, data.Length, endPoint);
            receiveCheck(i, endPoint);
             Addresses.RemoveAt(i);
             Ports.RemoveAt(i);
             kickUser.RemoveAt(i);
             tbText.AppendLine("Removed abusive user...");
             dataString = "CHAT\nUser " + endPoint.Address.ToString() + ":" + endPoint.Port.ToString() + " has been kicked!";
            }
		if (data == tempData && EndPoint.Address.Equals(Addresses[i]) && EndPoint.Port == Ports[i])
            {
             kickUser[i]++;
            }	
		}
        private void receiveData()
        {
            tempData = udpServer.Receive(ref EndPoint); // listen on port 11000


            if (tempData != null && Enumerable.SequenceEqual(tempData, data) == false)
            {
                data = tempData;
                tempData = new byte[0];
                dataString = byte2String(data);
                dataArray = new string[dataString.Split('\n').Count()];
                dataArray = dataString.Split('\n');

            }
            else
            {
                //   data = string2Byte(Convert.ToString(EndPoint.Address) + ":" + Convert.ToString(EndPoint.Port) + " is being a bad sport...");
                tempData = data;
            }
        }
    }
}
