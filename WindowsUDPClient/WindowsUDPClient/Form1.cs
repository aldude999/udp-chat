﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace WindowsUDPClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AcceptButton = button1;
            
        }
        System.Net.Sockets.UdpClient client;
        System.Net.IPEndPoint ep;

        StringBuilder tbText = new StringBuilder();
        public Thread childReceiver;
        byte[] data = new byte[0];
        bool connected = false;
        Int32 BadTicks = 0;
        string[] dataArray = new string[1];
        List<string> userListString = new List<string>();
        private void button1_Click(object sender, EventArgs e)
        {
            if (connected == true)
            {
                try
                {
                    byte[] stringConverter = Encoding.UTF8.GetBytes("CHAT\n"+textBox4.Text + ": " + textBox1.Text);



                    // send data
                    client.Send(stringConverter, stringConverter.Length);

                    // then receive data
                    //   var receivedData = client.Receive(ref ep);

                    // toolStripStatusLabel1.Text = "Data sent successfully to: " + ep.ToString();
                    textBox1.Text = "";
                }
                catch
                {
                    connected = false;
                }
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            byte[] receivedData;
            client = new System.Net.Sockets.UdpClient();
            ep = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(textBox3.Text), Convert.ToInt32(textBox2.Text)); // endpoint where server is listening
            client.Client.ReceiveTimeout = 1000;
            client.Connect(ep);
            byte[] connectString = string2Byte("CONNECT FRM V.4\n"+textBox4.Text);
            client.Send(connectString, connectString.Length);
            connectString = string2Byte("CHAT\nClient " + textBox4.Text + " connected!");
            client.Send(connectString, connectString.Length);
        

            
            childReceiver = new Thread(multiThread);
            childReceiver.Start();

            button2.Enabled = false;
            button3.Enabled = true;
            timer1.Interval = 400;
            timer1.Start();
 
        }
        private string byte2String(byte[] convertMe) 
        {
            return Encoding.UTF8.GetString(convertMe);
        }
        private byte[] string2Byte(string convertMe)
        {
            return Encoding.UTF8.GetBytes(convertMe);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            byte[] connectString = string2Byte("CHAT\nClient " + textBox4.Text + " disconnected!");
            client.Send(connectString, connectString.Length);
            connectString = string2Byte("DISCONNECT");
            client.Send(connectString, connectString.Length);
            client.Close();
            button2.Enabled = true;
            button3.Enabled = false;
            toolStripStatusLabel1.Text = "Disconnected!";
            tbText.AppendLine("Disconnected from server!");
            textBox5.Text = tbText.ToString();
            timer1.Stop();
            childReceiver.Abort();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (connected == true)
            {
                toolStripStatusLabel1.Text = "Connected to server: " + ep.Address.ToString() + ":" + ep.Port.ToString();
                BadTicks = 0;
            }
            else
            {
                toolStripStatusLabel1.Text = "Connecting to server...";
                BadTicks++;
                if (BadTicks >= 8)
                {
                    BadTicks = 0;
                    toolStripStatusLabel1.Text = "Could not connect to server!";
                    childReceiver.Abort();
                    button2.Enabled = true;
                    button3.Enabled = false;
                    tbText.AppendLine("Error connecting to server!");
                    textBox5.Text = tbText.ToString();
                    timer1.Stop();
                }
            }
            if (textBox5.Text != tbText.ToString())
            {
                textBox5.Text = tbText.ToString();
                textBox5.Invalidate();
                textBox5.Update();
                textBox5.Refresh();
                textBox5.SelectionStart = textBox5.TextLength;
                textBox5.ScrollToCaret();
                Application.DoEvents();
            }
            if (listBox1.Items.Count != userListString.Count)
            {
                listBox1.Items.Clear();
                for (Int32 i = 0; i <= userListString.Count - 1; i++)
                {
                    listBox1.Items.Add(userListString[i]);
                }
            }
        }
        private void multiThread()
        {
            bool working = true;
            while (working) {
            try
            {
                byte[] data = client.Receive(ref ep);
                client.Send(string2Byte("RCVD"), string2Byte("RCVD").Length);
                createArray(byte2String(data));

                if (data.Length != new byte[0].Length)
                {
                    connected = true;
                }
                if (dataArray[0] == "DISCONNECT")
                {
                    connected = false;
                    working = false;
                    tbText.AppendLine("Forcefully disconnected from server!");
                }
                if (dataArray[0] == "PONG")
                {
                    tbText.AppendLine(System.DateTime.Now + ": " + "Received PONG packet!");
                }
                if (dataArray[0] == "PING")
                {
                    tbText.AppendLine(System.DateTime.Now + ": " + "Received PING packet!");
                    tbText.AppendLine(System.DateTime.Now + ": " + "Sending PONG packet!");
                    pong();
                }
                if (dataArray[0] == "CHAT")
                {
                    tbText.AppendLine(System.DateTime.Now + ": " + dataArray[1]);
                }
                if (dataArray[0] == "LIST")
                {
                    userListString.Clear();
                    for (Int32 i = 1; i <= dataArray.Count()-1; i++)
                    {
                        userListString.Add(dataArray[i]);
                    }
                }

            }
            catch (Exception ex) { }
            }
            client.Close();
        }
        private void createArray(string Text)
        {
            dataArray = new string[Text.Split('\n').Count()];
            dataArray = Text.Split('\n');
        }
        private bool ping()
        {
            tbText.AppendLine(System.DateTime.Now + ": " + "Sending PING packet!");
            try
            {
                client.Send(string2Byte("PING"), string2Byte("PING").Length);
                return true;
            }
            catch { return false; }
        }
        private void pong()
        {
            client.Send(string2Byte("PONG"), string2Byte("PONG").Length);
        }

        private void pingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (connected == true)
            {
                ping();
            }
            else
            {
                MessageBox.Show("Please connect to a server!");
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                client.Close();
                childReceiver.Abort();
            }
            catch { }
            this.Close();
        }

        private void getUserListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (connected == true)
            {
                listBox1.Items.Clear();
                listUsers();
            }
            else
            {
                MessageBox.Show("Please connect to a server!");
            }
        }
        private bool listUsers()
        {
           
                tbText.AppendLine(System.DateTime.Now + ": " + "Sending LIST command!");
                try
                {
                    client.Send(string2Byte("LIST"), string2Byte("LIST").Length);
                    return true;
                }
                catch { return false; }
            
        }
    }
}
